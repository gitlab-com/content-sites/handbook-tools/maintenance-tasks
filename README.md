# Maintenance tasks

Project to house maintenance tasks issues, and related files.

For documentation on maintenance, please see: https://handbook.gitlab.com/docs/development/maintenance/

## Auto-generated Issues Usage

This code is heavily modified and simplified from https://gitlab.com/gitlab-com/Product.


## Testing Locally

To test the `create_issues` script use the `--dry-run` argument. Before running, make sure you set the `GITLAB_API_PRIVATE_TOKEN` environment variable.

```
bundle exec ruby create_issues.rb --dry-run
```

## Limitations

* In order to use multiple quick actions you have to add an extra line space between them
