# spec/compress_images_spec.rb

require 'rspec'
require 'fileutils'

# Mock the ExecCommands module
module ExecCommands
  def self.exec_log_cmd(cmd)
    # Mock implementation
  end

  def self.exec_cmd(cmd)
    # Mock implementation
  end

  def self.configure_git
    # Mock implementation
  end
end

RSpec.describe 'CompressImages' do
  let(:project_name) { 'test-project' }
  let(:gitlab_token) { 'dummy_token' }
  let(:gitlab_user) { 'test_user' }
  let(:branch_name_pattern) { /compress-images-\d{14}/ }

  # Create a temporary directory
  let(:temp_dir) { File.join(Dir.pwd, 'tmp_test_dir') }
  let(:project_dir) { File.join(temp_dir, project_name) }

  before do
    # Create a flexible ENV stub
    env_values = {
      'GITLAB_TOKEN' => gitlab_token,
      'GITLAB_USER_LOGIN' => gitlab_user
    }

    allow(ENV).to receive(:[]) do |key|
      env_values[key]
    end

    # Create the temp directory and project directory structure
    FileUtils.mkdir_p(File.join(project_dir, 'content/images'))

    # Mock Dir.chdir to handle block-based usage
    allow(Dir).to receive(:chdir) do |dir, &block|
      # If a block is given, yield to it
      block.call if block
    end

    # Define the create_mr_with_compressed_images method for testing
    def create_mr_with_compressed_images(project_name)
      url = "https://oauth2:#{ENV['GITLAB_TOKEN']}@gitlab.com/gitlab-com/content-sites/#{project_name}.git"

      ExecCommands.exec_log_cmd("git clone #{url}")

      # Using Dir.chdir with a block
      Dir.chdir(project_name) do
        branch_name = "compress-images-#{Time.now.strftime('%Y%m%d%H%M%S')}"
        ExecCommands.exec_log_cmd("git checkout -b #{branch_name}")

        # Compression commands
        ExecCommands.exec_log_cmd("find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt")
        ExecCommands.exec_log_cmd("find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt")
        ExecCommands.exec_log_cmd("find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt")

        # Check if there are any changes to commit
        status_output = ExecCommands.exec_cmd("git status --porcelain")
        if status_output.empty?
          puts "No changes to commit."
        else
          ExecCommands.exec_log_cmd("git add .")
          commit_message = "Compress png jpg jpeg images"
          ExecCommands.exec_log_cmd("git commit -m \"#{commit_message}\"")
          ExecCommands.exec_log_cmd("git push origin #{branch_name}")
          ExecCommands.exec_log_cmd("glab auth status")
          ExecCommands.exec_log_cmd("glab mr create -f -y -a #{ENV['GITLAB_USER_LOGIN']} --remove-source-branch --squash-before-merge --label Handbook::Operations")
        end
      end
    end

    # Mock the script loading
    allow_any_instance_of(Object).to receive(:load).with('scripts/compress_images.rb') do
      # Define the projects array
      projects = ['handbook', 'internal-handbook', 'docsy-gitlab']

      # Configure git
      ExecCommands.configure_git

      # Process each project
      projects.each do |project|
        create_mr_with_compressed_images(project)
      end
    end
  end

  after do
    # Clean up the temporary directory
    FileUtils.remove_entry(temp_dir) if File.directory?(temp_dir)
  end

  describe '#create_mr_with_compressed_images' do
    context 'when there are changes to commit after compression' do
      before do
        # Mock git status to show changes
        allow(ExecCommands).to receive(:exec_cmd)
          .with('git status --porcelain')
          .and_return('M content/images/large-image.png')
      end

      it 'executes all expected commands' do
        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("git clone https://oauth2:#{gitlab_token}@gitlab.com/gitlab-com/content-sites/#{project_name}.git")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with(match(/git checkout -b compress-images-\d{14}/))

        # Compression commands
        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt")

        # Git commands for committing changes
        expect(ExecCommands).to receive(:exec_log_cmd)
          .with('git add .')

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with('git commit -m "Compress png jpg jpeg images"')

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with(match(/git push origin compress-images-\d{14}/))

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with('glab auth status')

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("glab mr create -f -y -a #{gitlab_user} --remove-source-branch --squash-before-merge --label Handbook::Operations")

        create_mr_with_compressed_images(project_name)
      end
    end

    context 'when there are no changes to commit after compression' do
      before do
        # Mock git status to show no changes
        allow(ExecCommands).to receive(:exec_cmd)
          .with('git status --porcelain')
          .and_return('')
      end

      it 'does not create merge request' do
        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("git clone https://oauth2:#{gitlab_token}@gitlab.com/gitlab-com/content-sites/#{project_name}.git")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with(match(/git checkout -b compress-images-\d{14}/))

        # Compression commands
        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt")

        expect(ExecCommands).to receive(:exec_log_cmd)
          .with("find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt")

        # Should not execute commit commands if no changes
        expect(ExecCommands).not_to receive(:exec_log_cmd)
          .with('git add .')

        expect(ExecCommands).not_to receive(:exec_log_cmd)
          .with('git commit -m "Compress png jpg jpeg images"')

        # Capture the puts output to verify the no changes message
        expect { create_mr_with_compressed_images(project_name) }
          .to output(/No changes to commit/).to_stdout

        # REMOVED THE DUPLICATE CALL HERE
      end
    end
  end

  describe 'project iteration' do
    it 'processes all projects in the list' do
      # Set up expectations for each project
      expect(self).to receive(:create_mr_with_compressed_images).with('handbook')
      expect(self).to receive(:create_mr_with_compressed_images).with('internal-handbook')
      expect(self).to receive(:create_mr_with_compressed_images).with('docsy-gitlab')

      # Configure git mock
      allow(ExecCommands).to receive(:configure_git)

      # Run the script
      load 'scripts/compress_images.rb'
    end
  end

  describe 'compression commands' do
    it 'uses correct size thresholds for different image types' do
      # Create test files
      png_command = "find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt"
      jpg_command = "find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt"
      jpeg_command = "find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt"

      # Verify PNG command uses 100k threshold
      expect(png_command).to include("-size +100k")

      # Verify JPG and JPEG commands use 50k threshold
      expect(jpg_command).to include("-size +50k")
      expect(jpeg_command).to include("-size +50k")

      # Verify correct compression tools are used
      expect(png_command).to include("pngquant --ext .png --force --skip-if-larger")
      expect(jpg_command).to include("jpegoptim -t -T5")
      expect(jpeg_command).to include("jpegoptim -t -T5")
    end
  end
end
