# This code is heavily simplified from https://gitlab.com/gitlab-com/Product/-/blob/main/create_issues.rb?ref_type=heads to only serve a narrow use case. If we ever want to expand on what use cases are served, we can refactor this code to reconsider adding that all back in.

require 'gitlab'
require 'date'

dry_run = false
today = Date.today
project_id = 59095325
api_token = ENV['GITLAB_API_PRIVATE_TOKEN']

class IssueCreator
  def initialize(today)
    @today = today
  end

  def run(api_token, project_id)
    connect(api_token)

    today_day = @today.strftime('%d')
    today_month = @today.strftime('%m')
    puts "** Today is day #{today_day} of month #{today_month}"
    
    name = "[#{@today.cwyear}-#{today_month}] Monthly Handbook Maintenance Tasks"
    description = File.open('.gitlab/issue_templates/Default.md').read
    puts '** Description generated...'
    @gitlab.create_issue(project_id, name, { description: description, assignee_id: @user.id })
    puts "** #Monthly Handbook Maintenance Tasks issue created."
    puts 'Run Complete.'
  end

  def connect(api_token)
    puts 'Connecting to GitLab...'
    @gitlab = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: api_token
    )
    @user = @gitlab.user
    puts 'Connection successful. Connected user email: ' << @user.email
  end
end

# Mock GitLab user and service
class UserMock
  def initialize
    @email = 'mockuser@gitlab.com'
    @id = 1234
  end
  attr_accessor :email, :id
end
class GitLabMock
  def initialize
    @user = UserMock.new()
  end

  def create_issue(project, title, options)
    puts 'Would have created issue:'
    puts "  Project: #{project}"
    puts "  Title: #{title}"
    puts "  Content: \n#{options[:description]}\n\n"
  end
  attr_accessor :user
end
# Override issue creator to use GitLab mock
class IssueCreatorTest < IssueCreator
  def connect(api_token)
    puts 'Connecting to GitLab...'
    @gitlab = GitLabMock.new()
    @user = @gitlab.user
    puts 'Connection successful. Connected user email: ' << @user.email
  end
end

if $PROGRAM_NAME == __FILE__
  # Override with mock if we are doing a dry run
  ARGV.each do |arg|
    if arg == '--dry-run'
      dry_run = true
      api_token = 'mocktoken'
      project_id = 'mockproject'
      (1..12).each do |m|
        # Simulate every month of the year
        (1..28).each do |d|
          # Simulate every day of the month (limit at 28 for Feb)
          today = Date::strptime("#{d}-#{m}-#{Date.today.cwyear}", '%d-%m-%Y')
          if today
            issue_creator = IssueCreatorTest.new(today)
            issue_creator.run(api_token, project_id)
          end
        end
      end
    end
  end
  unless dry_run
    issue_creator = IssueCreator.new(today)
    issue_creator.run(api_token, project_id)
  end
end
