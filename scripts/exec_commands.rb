#!/usr/bin/env ruby

require 'open3'

module ExecCommands
  def self.exec_cmd(cmd)
    output, stderr, status = Open3.capture3(cmd)
    raise "#{stderr} when executing #{cmd}" unless status.success?
    output
  end

  def self.exec_log_cmd(cmd)
    puts "Executing #{cmd}..."
    Open3.popen3(cmd) do |stdin, stdout, stderr, thread|
      out_thread = Thread.new do
        until (line = stdout.gets).nil? do
          puts "#{line}"
        end
      end

      err_thread = Thread.new do
        until (line = stderr.gets).nil? do
          puts " #{line}"
        end
      end

      out_thread.join
      err_thread.join
      exit_status = thread.value
      puts "Command Failed" unless exit_status.success?
    end
  end

  def self.configure_git
   if ENV["CI"]
     exec_log_cmd("git config --global user.email \"$GITLAB_USER_EMAIL\"")
     exec_log_cmd("git config --global user.name \"$GITLAB_USER_NAME\"")
   end
  end
end
