require 'gitlab'
require 'date'
require 'optparse'

# Parse command line options
options = {dry_run: false}
OptionParser.new do |opts|
  opts.banner = "Usage: ruby script.rb [options]"
  opts.on("-i", "--id PROJECT_ID", Integer, "Project ID") do |i|
    options[:project_id] = i
  end
  opts.on("--dry-run", "Perform a dry run without deleting branches") do
    options[:dry_run] = true
  end
end.parse!

if options[:project_id].nil?
  puts "Please specify a project ID with -i or --id"
  exit
end

# Configure the GitLab client
Gitlab.configure do |config|
  config.endpoint = ENV['CI_API_V4_URL'] || 'https://gitlab.com/api/v4'
  config.private_token = ENV['GITLAB_API_TOKEN']
end

project_id = options[:project_id]

three_months_ago = Date.today - 90
one_month_ago = Date.today - 30

branches = []
page = 1
loop do
  batch = Gitlab.branches(project_id, page: page, per_page: 100)
  filtered_batch = batch.select { |branch| Date.parse(branch.commit.created_at) < one_month_ago }
  branches.concat(filtered_batch)
  break if batch.size < 100  # Break if we've reached the last page
  page += 1
end

branches.each do |branch|
  # Skip protected branches
  next if branch.protected

  # Fetch all MRs for this branch
  mrs = Gitlab.merge_requests(project_id, source_branch: branch.name, state: 'all')

  if mrs.empty?
    # Case 1: Branch is not tied to any MR (use 3 months)
    last_commit_date = Date.parse(branch.commit['committed_date'])
    if last_commit_date < three_months_ago
      puts "#{options[:dry_run] ? '[DRY RUN] Would delete' : 'Deleting'} stale branch (no MR, no commits for 3 months): #{branch.name}"
      Gitlab.delete_branch(project_id, branch.name) unless options[:dry_run]
    end
  else
    # Case 2: Branch has MRs - check if any are open
    has_open_mr = mrs.any? { |mr| mr.state == 'opened' }
    # Skip if there's an open MR
    next if has_open_mr
    
    # If all MRs are closed/merged, proceed with deletion check
    mr = mrs.first
    if mr.state == 'closed' || mr.state == 'merged'
      mr_closed_date = Date.parse(mr.closed_at || mr.merged_at)
      if mr_closed_date < one_month_ago
        puts "#{options[:dry_run] ? '[DRY RUN] Would delete' : 'Deleting'} stale branch (closed/merged MR for 1 month): #{branch.name}"
        Gitlab.delete_branch(project_id, branch.name) unless options[:dry_run]
      end
    end
  end
end

puts "Stale branch cleanup #{options[:dry_run] ? 'dry run' : 'execution'} completed for project ID: #{project_id}"
