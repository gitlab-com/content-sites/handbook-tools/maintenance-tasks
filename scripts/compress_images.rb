#!/usr/bin/env ruby

require 'open3'
require_relative 'exec_commands'

def create_mr_with_compressed_images(project_name)
  url = "https://oauth2:#{ENV['GITLAB_TOKEN']}@gitlab.com/gitlab-com/content-sites/#{project_name}.git"

  ExecCommands.exec_log_cmd("git clone #{url}")
  Dir.chdir(project_name)

  branch_name = "compress-images-#{Time.now.strftime('%Y%m%d%H%M%S')}"
  ExecCommands.exec_log_cmd("git checkout -b #{branch_name}")

  ExecCommands.exec_log_cmd("find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt")
  ExecCommands.exec_log_cmd("find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt")
  ExecCommands.exec_log_cmd("find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt")

  # Check if there are any changes to commit
  status_output = ExecCommands.exec_cmd("git status --porcelain")
  if status_output.empty?
    puts "No changes to commit."
  else
    ExecCommands.exec_log_cmd("git add .")
    commit_message = "Compress png jpg jpeg images"
    ExecCommands.exec_log_cmd("git commit -m \"#{commit_message}\"")
    ExecCommands.exec_log_cmd("git push origin #{branch_name}")
    ExecCommands.exec_log_cmd("glab auth status")
    ExecCommands.exec_log_cmd("glab mr create -f -y -a #{ENV['GITLAB_USER_LOGIN']} --remove-source-branch --squash-before-merge --label Handbook::Operations")
  end

  # Move back up in case we're cloning another project
  Dir.chdir('..')
end

# Configure local git user
ExecCommands.configure_git

projects = ["handbook", "internal-handbook","docsy-gitlab"]

projects.each do |project|
  create_mr_with_compressed_images(project)
end
