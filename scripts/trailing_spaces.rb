#!/usr/bin/env ruby

require 'open3'
require_relative 'exec_commands'

def create_mr_with_trailing_spaces_fix(project_name)
  url = "https://oauth2:#{ENV['GITLAB_TOKEN']}@gitlab.com/gitlab-com/content-sites/#{project_name}.git"

  ExecCommands.exec_log_cmd("git clone #{url}")
  Dir.chdir(project_name)

  branch_name = "trailing-spaces-#{Time.now.strftime('%Y%m%d%H%M%S')}"
  ExecCommands.exec_log_cmd("git checkout -b #{branch_name}")

  # Renames the markdownlint config, otherwise, it will try using the outputters listed even though
  ExecCommands.exec_log_cmd("mv -f .markdownlint-cli2.jsonc .markdownlint-cli2.config")
  ExecCommands.exec_log_cmd("markdownlint-cli2 --config ../task_files/task.markdownlint-cli2.jsonc 'content/**/*.md' --fix")
  # Renames the original markdownlint config back, otherwise, it will be a change to commit
  ExecCommands.exec_log_cmd("mv -f .markdownlint-cli2.config .markdownlint-cli2.jsonc")

  # Check if there are any changes to commit
  status_output = ExecCommands.exec_cmd("git status --porcelain")
  if status_output.empty?
    puts "No changes to commit."
  else
    ExecCommands.exec_log_cmd("git add --verbose .")
    commit_message = "Remove trailing spaces"
    ExecCommands.exec_log_cmd("git commit -m \"#{commit_message}\"")
    ExecCommands.exec_log_cmd("git push origin #{branch_name}")
    ExecCommands.exec_log_cmd("glab auth status")
    ExecCommands.exec_log_cmd("glab mr create -f -y -a #{ENV['GITLAB_USER_LOGIN']} --remove-source-branch --squash-before-merge --label Handbook::Operations")
  end

  # Move back up in case we're cloning another project
  Dir.chdir('..')
end

# Configure local git user
ExecCommands.configure_git

projects = ["handbook", "internal-handbook"]

projects.each do |project|
  create_mr_with_trailing_spaces_fix(project)
end
