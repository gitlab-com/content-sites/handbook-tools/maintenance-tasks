#!/usr/bin/env ruby

require 'open3'
require 'shellwords'
require_relative 'exec_commands'

def find_unlinked_media(directory)
  unlinked_media = []
  media_files = Dir.glob(File.join(directory, '**', '*.{png,gif,jpg,jpeg,svg,pdf,mov,mp4,m4v,avi,mkv,ogg,webm}'))
  excluded_files = [
    'content/featured-background.png',
    'static/apple-touch-icon-1024x1024.png',
    'static/handbook-icon_favicon.png',
    'static/macOS-handbook-icon-alt.png',
    'static/images/slack-logo.png',
    'static/images/slack-mark.svg'
  ]
  excluded_folders = [
   'assets',
   'static/favicons',
   'static/images/all-remote',
   'static/images/maturity',
   'static/images/sdlc-icons'
  ]

  media_files.each do |file|
    next if excluded_files.any? { |excluded| file.end_with?(excluded) }
    next if excluded_folders.any? { |folder| file.include?("/#{folder}/") }
    basename = File.basename(file)
    escaped_basename = Shellwords.escape(basename)
    space_encoded_basename = basename.gsub(' ', '%20')
    escaped_space_encoded_basename = Shellwords.escape(space_encoded_basename)

    is_linked = system("git grep -q #{escaped_basename}", out: File::NULL, err: File::NULL) ||
                system("git grep -q #{escaped_space_encoded_basename}", out: File::NULL, err: File::NULL)

    unless is_linked
      unlinked_media << file
    end
  end

  unlinked_media
end

def create_mr_with_unlinked_media(project_name)
  url = "https://oauth2:#{ENV['GITLAB_TOKEN']}@gitlab.com/gitlab-com/content-sites/#{project_name}.git"

  ExecCommands.exec_log_cmd("git clone #{url}")
  Dir.chdir(project_name)

  unlinked_list = find_unlinked_media('.')
  if unlinked_list.empty?
    puts "No media files to remove. Exiting..."
    exit 0
  else
    puts "List of unlinked files:"
    unlinked_list.each { |file| puts file }
  end

  branch_name = "remove-unused-media-#{Time.now.strftime('%Y%m%d%H%M%S')}"
  ExecCommands.exec_log_cmd("git checkout -b #{branch_name}")

  unlinked_list.each do |file|
    escaped_file = Shellwords.escape(file)
    ExecCommands.exec_log_cmd("git rm #{escaped_file}")
  end

  # Check if there are any changes to commit
  status_output = ExecCommands.exec_cmd("git status --porcelain")
  if status_output.empty?
    puts "No changes to commit."
  else
    ExecCommands.exec_log_cmd("git add .")
    commit_message = "Remove unused media"
    ExecCommands.exec_log_cmd("git commit -m \"#{commit_message}\"")
    ExecCommands.exec_log_cmd("git push origin #{branch_name}")
    ExecCommands.exec_log_cmd("glab auth status")
    ExecCommands.exec_log_cmd("glab mr create -f -y -a #{ENV['GITLAB_USER_LOGIN']} --remove-source-branch --squash-before-merge --label Handbook::Operations")
  end

  # Move back up in case we're cloning another project
  Dir.chdir('..')
end

# Configure local git user
ExecCommands.configure_git

projects = ["handbook", "internal-handbook"]

projects.each do |project|
  create_mr_with_unlinked_media(project)
end
