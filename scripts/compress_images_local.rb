#!/usr/bin/env ruby

require 'open3'
require_relative 'exec_commands'
require 'optparse'

def create_mr_with_compressed_images(project_name, gitlab_user_login)
  # Assume project directory exists and move into it
  Dir.chdir(project_name)
  puts "Working on #{project_name}..."

  ExecCommands.exec_log_cmd("git checkout main")
  ExecCommands.exec_log_cmd("git pull origin main")

  branch_name = "compress-images-#{Time.now.strftime('%Y%m%d%H%M%S')}"
  ExecCommands.exec_log_cmd("git checkout -b #{branch_name}")

  ExecCommands.exec_log_cmd("find . -name '*.png' -size +100k > temp_png_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && pngquant --ext .png --force --skip-if-larger \"$file\" || echo \"Skipping $file\"; done < temp_png_list.txt && rm temp_png_list.txt")
  ExecCommands.exec_log_cmd("find . -name '*.jpg' -size +50k > temp_jpg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpg_list.txt && rm temp_jpg_list.txt")
  ExecCommands.exec_log_cmd("find . -name '*.jpeg' -size +50k > temp_jpeg_list.txt && while IFS= read -r file; do echo \"Processing: $file\" && jpegoptim -t -T5 \"$file\" || echo \"Skipping $file\"; done < temp_jpeg_list.txt && rm temp_jpeg_list.txt")

  # Check if there are any changes to commit
  status_output = ExecCommands.exec_cmd("git status --porcelain")
  if status_output.empty?
    puts "No changes to commit. Exiting..."
    exit 0
  end

  ExecCommands.exec_log_cmd("git add .")
  commit_message = "Compress png jpg jpeg images"
  ExecCommands.exec_log_cmd("git commit -m \"#{commit_message}\"")
  ExecCommands.exec_log_cmd("git push origin #{branch_name}")
  ExecCommands.exec_log_cmd("glab auth status")
  ## gitlab_user_login is username without @ in front
  ExecCommands.exec_log_cmd("glab mr create -f -y -a #{gitlab_user_login} --remove-source-branch --squash-before-merge --label Handbook::Operations")

  # Move back to parent directory
  parent_dir = File.dirname(Dir.pwd)
  Dir.chdir(parent_dir)
end

# Parse command line options
options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: ruby compress_images.rb [options]"

  opts.on("-u", "--user USERNAME", "GitLab username") do |u|
    options[:gitlab_user_login] = u
  end
end.parse!

# Check if GitLab username is provided
if options[:gitlab_user_login].nil?
  puts "Error: GitLab username is required. Use -u or --user option."
  exit 1
end

projects = ["handbook", "internal-handbook","docsy-gitlab"]

projects.each do |project|
  create_mr_with_compressed_images(project, options[:gitlab_user_login])
end
