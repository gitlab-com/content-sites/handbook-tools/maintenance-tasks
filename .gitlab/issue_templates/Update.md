<!-- Title: 202Y-MM-DD Handbook update -->
/due 202Y-MM-DD
/milestone %
/label ~"Handbook::Updates"
/assign me

Handbook update since [milestone or date] up to the end of [milestone and date].

Previous update: [issue number]

Task: List any "significant" issues or MRs (at your discretion), and generally all contributions. Before closing, create next update issue.

## Issues completed

Full list of issues: https://gitlab.com/groups/gitlab-com/content-sites/-/issues/?sort=closed_at_desc&state=closed&label_name%5B%5D=Handbook%3A%3AOperation

1. [issue link and contributor]

## MRs

Full list of MRs: https://gitlab.com/groups/gitlab-com/content-sites/-/merge_requests?label_name%5B%5D=Handbook%3A%3AOperations&scope=all&sort=merged_at_desc&state=merged

Not covered by issues:

1. [MR link and contributor]

## Update to post

Post in `#whats-happening-at-gitlab` and cross-post to `#handbook`.

```
:gl-handbook: :mega: *202Y-MM-DD Handbook update*

INSERT MESSAGE

:mr: Those are just the highlights. You can see a full list in [the handbook update issue](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Handbook%253A%253AUpdates). To see regular updates as they happen, come join #handbook! :slack:

:hero: If you'd like to contribute, please take a look at [the epic of issues to be done](https://gitlab.com/groups/gitlab-com/content-sites/-/epics/12). :pray: 
```

### Contribution thanks

Post in `#thanks`, and remember to cc managers.

```
:gl-handbook: :ty_thankyou: *Today's edition of Handbook Contributions Thanks*

Thanks as always to @darbyfrey for leading our backend work. And to <insert names> for contributing MRs to improve our handbook! :collaboration-tanuki: :results-tanuki: :efficiency-tanuki: 

See [the latest handbook update issue](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/issues/) for a list of issues and MRs!

cc [managers]
```