<!-- Title: [YYYY-MM] Monthly Handbook Maintenance Tasks -->
/label ~"Handbook::Operations" ~"type::maintenance"
/assign @jallen16 @tely2 

**Reminder**: Typically, all maintenance MRs should have the current milestone, and be labelled with ~"Handbook::Operations" ~"type::maintenance" and other labels as appropriate.

Documentation: https://handbook.gitlab.com/docs/development/maintenance/

## General tasks

1. [ ] Set the `Due date` to the last working day of this month
1. [ ] Set the milestone to the one corresponding to the due date

## Content tasks

1. [ ] Codeowners check
   - [ ] Handbook
   - [ ] Internal handbook
1. [ ] Compress images
   - [ ] Handbook
   - [ ] Internal handbook
   - [ ] Docsy-theme
1. [ ] Check for unlinked images
   - [ ] Handbook
   - [ ] Internal handbook
1. [ ] Check for broken external links
   - [ ] Handbook
   - [ ] Internal handbook
   - [ ] Docsy-theme
1. [ ] Check for broken internal links
   - [ ] Handbook
1. [ ] Check for product docs linter rules changes
   - [ ] markdownlint: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.markdownlint-cli2.yaml
   - [ ] vale: https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/.vale
1. [ ] Run linters to double check errors are not being introduced
   - markdownlint
       - [ ] Handbook
       - [ ] Internal handbook
   - vale
       - [ ] Handbook
       - [ ] Internal handbook
1. [ ] Fix trailing spaces
   - [ ] Handbook
   - [ ] Internal handbook
1. [ ] Check for expired redirects
   - [ ] Handbook
   - [ ] Internal handbook
1. [ ] Add "other" tasks as necessary

## Backend tasks

1. [ ] Go tidy 
1. [ ] Review/bump dependencies version:
   - [ ] docsy
   - [ ] golang
   - [ ] hugo
   - [ ] nodejs, including package dependencies
   - [ ] markdownlint
   - [ ] vale
1. [ ] Upgrade dependencies
   - [ ] Docsy-theme
   - [ ] Handbook
   - [ ] Internal handbook